const bespoke = require('bespoke')
const bullets = require('./bespoke-bullets-patched')
const classes = require('bespoke-classes')
const cursor = require('bespoke-cursor')
const ensuite = require('./ensuite-protocol-bespoke')
const extern = require('bespoke-extern')
const fitText = require('./bespoke-fit-text')
const hash = require('bespoke-hash')
const multimedia = require('bespoke-multimedia')
const nav = require('bespoke-nav')
const overview = require('bespoke-overview')
const scale = require('./bespoke-scale-patched')

bespoke.from({ parent: 'article.deck', slides: 'section' }, [
  classes(),
  scale('transform'),
  nav(),
  overview(),
  bullets('.build, .build-items > *:not(.build-items)'),
  hash(),
  multimedia(),
  fitText(),
  // QUESTION should we put cursor behind a key combination?
  //cursor(3000),
  // QUESTION should we move activation logic into plugin?
  (/(^\?|&)ensuite(?=$|&)/.test(window.location.search) ? ensuite() : () => {}),
  extern(bespoke),
])
